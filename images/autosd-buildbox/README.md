# AutoSD BuildBox Image

Container image to be used as RPM build environment for AutoSD packages.

This image can be used to create a toolbox environment.

## Instructions

The image isolates its rpmbuild environemnt in `/opt/autosd/rpmbuild` instead of using the default `$HOME/rpmbuild` location so
it can be used as a [toolbox](https://containertoolbx.org/).

A `rpmbuild` alias is provided to use the custom rpmbuild home folder.

Th image is available to pulled from quay.io as `quay.io/centos-sig-automotive/autosd-buildbox:latest`.

You can build this image by running (in case you do not want to pull the existing one from quay.io):

```sh
podman build -t localhost/autosd-buildbox:latest .
```

You can start a new build box container by running:

```sh
# keep in mind that using "-rm" will delete an existing container with the same name, if any
podman run -rm -it --name autosd-buildbox-sample localhost/autosd-buildbox:latest /bin/bash
```

Or use the DEV environment instructions bellow

### Toolbox

Create a new toolbox container by running:

```sh
toolbox create --image quay.io/centos-sig-automotive/autosd-buildbox:latest autosd-buildbox-sample
```

You can now enter into your new buildbox environment by running:

```sh
toolbox enter autosd-buildbox-sample
```

## DEV environment

Implemented with [devcontainers](https://containers.dev/)

To generate or update the images/autosd-buildbox rpms lockfile:

```shell
    cd images/autosd-buildbox
    make lock
```

To build the images/autosd-buildbox image:

```shell
    cd images/autosd-buildbox
    make build
```
