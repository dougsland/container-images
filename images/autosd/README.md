# AutoSD Dev Container

This container provides a base AutoSD environment for local application development and deployment using containers with:

* systemd
* bluechi (includes bluechictl)
* qm

## Building

Build the image by running:

```sh
podman build --cap-add=sys_admin -t localhost/autosd:latest .
```

Note: the container needs to be built using `--cap-add=sys_admin` because of the qm setup script.

## Running

```sh
podman run --rm -d --name autosd --privileged localhost/autosd:latest
```

## Using

You can "sh into the container" by running:

```
podman exec -it autosd /bin/bash
```

Check if both qm and no-qm agents are running:

```sh
systemctl status bluechi
bluechictl list-units
```

You can now experiment deploying your own quadlet/bluechi workloads.
