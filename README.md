# CentOS Automotive Container Images

This repostory contains all the required files to build linux container images
that are used in AutoSD OS images.

Images are pushed to `quay.io/centos-sig-automotive/$FOLDER_NAME:latest`.

## License

[MIT](./LICENSE)
