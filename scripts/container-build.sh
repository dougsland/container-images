#!/bin/bash
LIB_DIR="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )/lib"

source $LIB_DIR/git.sh
source $LIB_DIR/log.sh
source $LIB_DIR/env.sh
source $LIB_DIR/container.sh

if [ -z "${PATHS+x}" ]; then
    if [ -z "${CI_MERGE_REQUEST_DIFF_BASE_SHA+x}" ]; then
        REFS="$CI_MERGE_REQUEST_TARGET_BRANCH_NAME"
    else
        REFS=$CI_MERGE_REQUEST_DIFF_BASE_SHA
    fi
    PATHS=$(git::diff $REFS | grep images | grep -oE "^images/[a-zA-Z_\-]+" | uniq)
fi

log::info "Using refs: $REFS"

log::info "Identified changes:"
for path in $PATHS; do
    echo -e "\t$path"
done

for path in $PATHS; do
    if [ ! -f "$path/.ignoreme" ]; then
        env::load_vars $path/.ci/env

        log::info "Building $path..." 
        container::build $path
        log::info "$path built."

        env::unload_vars $path/.ci/env
    fi
done

log::info "Built images:"
for path in $PATHS; do
    echo -e "\t${IMG_REG}/${IMG_ORG}/$(basename $path):${IMG_TAG}"
done
