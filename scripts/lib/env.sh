env::load_vars() {
    local VARS_FILE=$1

    if [ -f "$VARS_FILE" ]; then
        source $VARS_FILE
    fi
}

env::unload_vars() {
    local VARS_FILE=$1

    if [ -f "$VARS_FILE" ]; then
        unset $(awk -F'[ =]+' '{print $1}' $VARS_FILE)
    fi
}
