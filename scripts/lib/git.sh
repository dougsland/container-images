# git::diff_tree "main"
git::diff() {
    local REFS=$1

    git diff --no-commit-id --name-only $REFS...HEAD
}
